| NUmber | Stages                 | Start date  |  End Date  | Commit                                                                                    |
| ------:| ---------------------- |:-----------:|:----------:|-------------------------------------------------------------------------------------------|
|      1 | Task Clarification     | 06.05.2023  | 08.05.2023 | Summarized the purpose of the course project.                                             |
|      2 | Analysis               | 09.05.2023  | 12.05.2023 | Got a handle on the requirements.                                                         |
|      3 | Use Cases              | 13.05.2023  | 16.05.2023 | The functionality of the commands is discussed.                                           |
|      4 | Search for Solutions   | 17.05.2023  | 25.05.2023 | Gave attention to how to implement controller, service, dao, entity, exception layers     |
|      5 | Software Development   | 04.06.2023  | 08.06.2023 | The project is almost done and I am now focusing on improving the performance of the code |
|      6 | Development Completion | not started | 09.06.2023 | 1 day left                                                                                |
|      7 | Presentation           | not started | 09.06.2023 | 1 day left                                                                                |