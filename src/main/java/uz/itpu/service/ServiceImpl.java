package uz.itpu.service;

import uz.itpu.entity.Toy;
import uz.itpu.repository.ProductRepository;
import uz.itpu.repository.ProductRepositoryImpl;

import java.util.ArrayList;
import java.util.List;

public class ServiceImpl implements Service{
    ProductRepository productRepository;
    public ServiceImpl(){
        this.productRepository=new ProductRepositoryImpl();
    }
    @Override
    public void getAllProducts() {
        productRepository.getAllProducts().forEach(System.out::println);
    }

    @Override
    public List<Toy> searchProductByName(String name) {
        return productRepository.searchProductByName(name);
    }

    @Override
    public List<Toy> searchProductByCategory(String category) {
        return productRepository.searchProductByCategory(category);
    }



    @Override
    public List<Toy> searchProductByPrice(String price) {
        return productRepository.searchProductByPrice(price);
    }


}
