package uz.itpu.service;

import uz.itpu.entity.Toy;

import java.util.List;

public interface Service {
    void getAllProducts();
    List<Toy> searchProductByName(String name);
    List<Toy> searchProductByCategory(String category);
    List<Toy> searchProductByPrice(String price);
}
