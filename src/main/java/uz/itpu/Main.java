package uz.itpu;

import uz.itpu.controller.Controller;
import uz.itpu.controller.ControllerImpl;

public class Main {
    public static void main(String[] args) {
        Controller controller = new ControllerImpl();
        controller.mainController();
    }
}