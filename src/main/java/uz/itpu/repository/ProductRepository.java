package uz.itpu.repository;

import uz.itpu.entity.Toy;

import java.util.ArrayList;

public interface ProductRepository {
    ArrayList<Toy> getAllProducts();
    ArrayList<Toy> searchProductByName(String name);
    ArrayList<Toy> searchProductByCategory(String category);
    ArrayList<Toy> searchProductByPrice(String price);
}
