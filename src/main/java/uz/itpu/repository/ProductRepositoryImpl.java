package uz.itpu.repository;

import uz.itpu.entity.Toy;
import uz.itpu.utils.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class ProductRepositoryImpl implements ProductRepository{

    ArrayList<Toy> products = new ArrayList<>();
    @Override
    public ArrayList<Toy> getAllProducts() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(Utils
                .fileUrl))){
            String line;
            while ((line = bufferedReader.readLine()) != null){
                String[] split = line.split(",");
                Toy product = new Toy(
                        Long.parseLong(split[0]),
                        split[1],
                        split[2],
                        split[3],
                        Integer.parseInt(split[4])
                );
                products.add(product);
            }
        }catch (IOException e){
            e.printStackTrace();
        }return products;
    }

    @Override
    public ArrayList<Toy> searchProductByName(String name) {
        ArrayList<Toy> products = new ArrayList<>();
        for (Toy product : getAllProducts()){
            if(product.getName().toUpperCase().contains(name.toUpperCase())){
                products.add(product);
            }
        }return products;
    }

    @Override
    public ArrayList<Toy> searchProductByCategory(String category) {
        ArrayList<Toy> products = new ArrayList<>();
        for (Toy product : getAllProducts()) {
            if (Objects.equals(product.getCategory().toUpperCase(), category.toUpperCase())){
                products.add(product);
            }
        }return products;
    }

    @Override
    public ArrayList<Toy> searchProductByPrice(String price) {
        ArrayList<Toy> products = new ArrayList<>();
        for (Toy product : getAllProducts()){
            if (product.getPrice().equals(price)) {
                products.add(product);
            }
        }return products;
    }
}
