package uz.itpu.controller;

import uz.itpu.entity.Toy;
import uz.itpu.service.Service;
import uz.itpu.service.ServiceImpl;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class ControllerImpl implements Controller {
    Service service;
    public ControllerImpl(){
        this.service=new ServiceImpl();
    }

    @Override
    public void mainController() {

        String info = """
                APP_NAME = Toys Warehouse
                Version = 1.0
                Creation date = 5/17/2023
                Developer name  = Diyorbek Abduganiyev
                Developer email = Diyorbek_Abduganiyev@student.itpu.uz
                """;
        System.out.println(info);
        String menu = """
                0. Exit
                1. View all products
                2. Searching product by name
                3. Searching product by price
                4. Searching product by category
                """;
        System.out.println(menu);
            Scanner scanner = new Scanner(System.in);
            while (true) {
                System.out.println(menu);
                System.out.println("Enter number between 0-4: ");
                try {
                    int command = scanner.nextInt();
                    scanner.nextLine();
                    switch (command) {
                        case 0 -> {
                            System.out.println("Bye!");
                            return;
                        }
                        case 1 -> {
                            service.getAllProducts();
                        }
                        case 2 -> {
                            searchProductByName();
                        }
                        case 3 -> {
                            searchProductByPrice();
                        }
                        case 4 -> {
                            searchProductByCategory();
                        }
                        default -> {
                            System.out.println("Wrong command");
                        }
                    }
                }catch (InputMismatchException e){
                    System.out.println("Please enter valid number!");
                    scanner.nextLine();
                }
            }
    }

    private void searchProductByPrice() {
        ServiceImpl service = new ServiceImpl();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Product price: ");
        String price = scanner.nextLine();
        List<Toy> products = service.searchProductByPrice(price);
        if(products.isEmpty()){
            System.out.println("Product not found.");
        }else {
            System.out.println("Matching Products");
            for (Toy product : products) {
                System.out.println(product);
            }
        }
    }


    private void searchProductByCategory() {
        ServiceImpl service = new ServiceImpl();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Product category: ");
        String productCategory = scanner.nextLine();
        List<Toy> products = service.searchProductByCategory(productCategory);
        if(products.isEmpty()){
            System.out.println("Product not found");
        }else{
            System.out.println("Matching not found");
            for (Toy product : products) {
                System.out.println(product);
            }
        }
    }


    private void searchProductByName(){
        ServiceImpl service = new ServiceImpl();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Product name: ");
        String productName = scanner.nextLine();
        List<Toy> products = service.searchProductByName(productName);
        if(products.isEmpty()){
            System.out.println("No products found");
        }else{
            System.out.println("Matching products");
            for (Toy product: products){
                System.out.println(product);
            }
        }
    }
}