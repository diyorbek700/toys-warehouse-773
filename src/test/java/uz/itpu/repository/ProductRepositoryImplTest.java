package uz.itpu.repository;

import org.junit.Before;
import org.junit.Test;
import uz.itpu.entity.Toy;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ProductRepositoryImplTest {

    private ProductRepositoryImpl productRepository;

    @Before
    public void setUp() { productRepository = new ProductRepositoryImpl();
    }

    @Test
    public void testGetAllProducts() {
        ArrayList<Toy> products = productRepository.getAllProducts();
        assertEquals(20, products.size());
    }

    @Test
    public void testSearchByProductName() {
        List<Toy> searchResults = productRepository.searchProductByName("Lego Classic Bricks");
        assertEquals(1, searchResults.size());
    }

    @Test
    public void testSearchByProductCategory() {
        ArrayList<Toy> searchResults = productRepository.searchProductByCategory("Construction");
        assertEquals(1, searchResults.size()); // Assuming there are 4 products in the "Living Room" category
    }

    @Test
    public void testSearchByProductPrice() {
        ArrayList<Toy> searchResults = productRepository.searchProductByPrice("19.99");
        assertEquals(2, searchResults.size()); // Assuming there are 14 products with a price less than or equal to 100.0
    }
}